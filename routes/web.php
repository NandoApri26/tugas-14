<?php

use App\Http\Controllers\CastController;
use App\Models\Cast;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template.main');
});

Route::get('/table', function(){
    return view(('table.index'));
});

Route::get('/data-table', function(){
    return view(('data-table.index'));
});


Route::resource('cast', CastController::class);