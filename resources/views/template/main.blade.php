<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>

    @include('template.header')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        @include('template.navbar')
        @include('template.sidebar')
    </div>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    @yield('content')
                </div>
            </div><!-- /.container-fluid -->
        </section>
    </div>
    @include('template.footer')
</body>

</html>
