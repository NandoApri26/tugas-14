@extends('template.main')
@section('title', 'Edit Cast')


@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height mt-5">
            <h3 class="ml-2"><b>Edit Cast</b></h3>
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form method="POST" action="{{url ('/cast/'.$cast->id)}}" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-2 mt-3">
                                            <label for="nama">Nama</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                                    id="nama" name="nama" value="{{ $cast->nama }}">
                                                @error('nama')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-2 mt-3">
                                            <label for="umur">Umur</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <input type="text"
                                                    class="form-control   @error('umur') is-invalid @enderror" id="umur"
                                                    name="umur" value="{{ $cast->umur }}">
                                                @error('umur')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-2 mt-3">
                                            <label for="bio">Bio</label>
                                        </div>
                                        <div class="col-10 mt-3">
                                            <div class="form-group">
                                                <textarea type="text" class="form-control @error('bio') is-invalid @enderror" placeholder="Masukkan beberapa kalimat"
                                                    id="bio" name="bio">{{ $cast->bio }}
                                                </textarea>
                                                @error('bio')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Kirim</button>
                                            <a href="{{ url('/cast') }}" class="btn btn-primary me-1 mb-1"
                                                role="button">
                                                Batal
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection