<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cast = Cast::all();
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required'
            ]
        );

        Cast::create(
            [
                'nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio,
            ]
        );

        return redirect('/cast')->with('status', 'Berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function show(Cast $cast)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function edit(Cast $cast)
    {
        return view('cast.update', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cast $cast)
    {
        // return $request;
        $request->validate(
            [
                'nama' => 'required | min : 3 | max : 25',
                'umur' => 'required',
                'bio' => 'required | min : 7 | max : 250',
            ],
            [
                'nama.required' => 'Nama harus di isi',
                'nama.min' => 'min 3 karakter',
                'nama.mix' => 'max 25 karakter',
                'umur.required' => 'Umur harus diisi',
                'bio.required' => 'Bio harus di isi',
                'bio.min' => 'min 3 karakter',
                'bio.max' => 'max 25 karakter'

            ]
        );
        Cast::where('id', $cast->id)->update(
            [
                'nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio,
            ]
            );
        return redirect('/cast')->with('status', 'Data berhasil di ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cast $cast)
    {
        Cast::destroy('id', $cast->id);
        return redirect('/cast');
    }
}
